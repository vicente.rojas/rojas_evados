<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Evaluacion 2</title>
        <link rel="stylesheet" href="css/bootstrap.css" type="text/css"/>
        <script type="text/javascript" src="js/alertas.js">

        </script>
    </head>
    <body background="Recursos/fondoori.png"style="background-size: cover">
        <h1 class="display-9"style="text-align: center"> Parque Jurásico</h1>
        <p style="text-align: center">Vicente Rojas</p>


        <div class="row" > 
            <div class="col-md-4">
                <h1 class="text-center">Brachiosaurio</h1>
                <div class="card">
                    <img class="card-img-top"src="Recursos/Brachio.jpg">
                    <div class="card-body"> 
                        <p class="card-text">Brachiosaurus altithorax es 
                            la única especie conocida del género extinto Brachiosaurus de dinosaurio saurópodo braquiosáurido, que vivió durante el Jurásico Superior, hace aproximadamente 154 y 153 millones de años </p>
                    
                    </div>
                    
                    <button type="button" class="btn btn-warning" onclick="document.getElementById('brachio').play();alerta4()">Prueba</button>   
                </div>
                <audio id="brachio" src="Recursos/brachiosaurus.mp3">  </audio>
            </div> 
            <div class="col-md-4">
                <h1 class="text-center">Velociraptor</h1>
                <div class="card">
                    <img class="card-img-top"src="Recursos/Raptor.jpg">
                     <div class="card-body"> 
                        <p class="card-text">Velociraptor  es un género de dinosaurios terópodos dromeosáuridos que vivieron durante el periodo Campaniaense, hacia finales 
                            del período Cretácico, hace unos 75 a 71 millones de años, en lo que es hoy Asia </p>
                    
                    </div>
                    <button type="button" class="btn btn-danger" onclick="document.getElementById('raptor').play();alerta3()">Prueba</button>   
                </div>
                <audio id="raptor" src="Recursos/velociraptor.mp3">  </audio>
            </div>
            <div class="col-md-4">
                <h1 class="text-center">Triceratops</h1>
                <div class="card">
                    <img class="card-img-top"src="Recursos/triceratops.jpg">
                     <div class="card-body"> 
                        <p class="card-text"> Triceratops o el tricerátops, es un género de dinosaurios ceratopsianos ceratópsidos, que vivieron a finales del período Cretácico, hace aproximadamente 68 y 
                            66 millones de años, en el Maastrichtiense, en lo que hoy es Norteamérica.</p>
                    
                    </div>
                    <button type="button" class="btn btn-warning" onclick="document.getElementById('tricera').play();alerta2()">Prueba</button>  
                </div>
                <audio id="tricera" src="Recursos/triceratop.mp3">  </audio>
            </div>

        </div>

        <div class="row">
            <div class="col-md-2">

            </div>
            <div class="col-md-4">
                <h1 class="text-center">Tyranosaurio</h1>
                <div class="card">
                    <img class="card-img-top"src="Recursos/trex.jpg">
                     <div class="card-body"> 
                        <p class="card-text">Tyrannosaurus rex, es la única especie conocida del género fósil Tyrannosaurus de dinosaurio terópodo tiranosáurido, que vivió a finales del período Cretácico, hace aproximadamente entre 68 y 66 millones de años, en el Maastrichtiense, en lo que es hoy América del Norte.</p>
                    
                    </div>
                    <button type="button" class="btn btn-danger" onclick="document.getElementById('trex').play();alerta1()">Prueba</button>  
                </div>
                <audio id="trex" src="Recursos/tyranosaurus.mp3">  </audio>

            </div>

            <div class="col-md-4">
                <h1 class="text-center">Spinosaurio</h1>
                <div class="card">
                    <img class="card-img-top"src="Recursos/Spino.jpg">
                     <div class="card-body"> 
                        <p class="card-text">Spinosaurus aegyptiacus es un género representado por una especie de dinosaurio terópodo espinosáurido, que vivió en lo que actualmente es el norte de África desde el Albiense Inferior hasta el Cenomaniense 
                            Inferior del periodo Cretácico, hace aproximadamente 112 a 93,5 millones de años. </p>
                    
                    </div>
                    <button type="button" class="btn btn-danger" onclick="document.getElementById('spino').play();alerta5()">Prueba</button>  
                </div>
                <audio id="spino" src="Recursos/spinosaurus.mp3">  </audio>

            </div>
            <div class="col-md-2">

            </div>
        </div>
    </body>
</html>
